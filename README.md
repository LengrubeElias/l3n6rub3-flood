# L3N6RUB3

_A simple Flood DDoS Attack_

#

# Requirements:
* Python 3.8.6 or higher
* Libraries already included in Python
#

# How to use it:
* Clone this repository
* Go to the clone folder
* Run: pip -r install requirements.txt
* Run: python l3n6rub3.py -s (ip) -p (port)
#

# Example of use:
* python l3n6rub3.py -s 192.168.0.2 -p 80
#

# Stop execution:
* CTRL + c
## l3n6rub3-flood
